import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userDetails: any[];

  constructor() {
  }

  ngOnInit() {
    this.userDetails = [
      {id: 'Full Name', value: 'Erinda Dane'},
      {id: 'Birthday', value: new Date('3/28/1995')},
      {id: 'Number of read books', value: '5'},
      {id: 'Member since', value: new Date('01/01/2017')},
      {id: 'Member until', value: new Date('01/01/2020')},
    ];
  }

}
