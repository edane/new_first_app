import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {getAll, SetAllUsers} from './users.actions';
import {UsersService} from './login/users.service';
import {map, switchMap} from 'rxjs/operators';

@Injectable()
export class UsersEffects {

  @Effect()
  getUser$ = this.action$.pipe(ofType(getAll),
    switchMap(() => this.usersService.getUsers()
      .pipe(map((res) => new SetAllUsers(res))))
  );

  constructor(private action$: Actions, private usersService: UsersService) {
  }
}
