import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient, private db: AngularFirestore) {
  }

  getUsers() {
    return this.db.collection('users').valueChanges();
    // return this.http.get<any[]>('assets/mocks/users.json').pipe(map(res => res));
  }

  addNewUser(user: object) {
    return this.db.collection('users').add(user);
  }
}
