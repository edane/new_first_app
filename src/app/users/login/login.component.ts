import {Component, OnInit, ViewChild} from '@angular/core';
import {UsersService} from './users.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {LibraryStateModel} from '../../shared/library-state/library-state.model';
import {Store} from '@ngrx/store';
import {GetAllUsers, StoreUserData} from '../users.actions';
import {getAllUsers, getUserData} from '../users.selector';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm: NgForm;

  constructor(private userService: UsersService, private route: Router,
              private store: Store<LibraryStateModel>) {
  }

  ngOnInit() {
    this.store.select(getUserData()).subscribe(res => {
      console.log(res);
    });
  }

  onSubmit() {
    console.log(this.loginForm);
    this.login(this.loginForm.value.username, this.loginForm.value.password);
    // this.loginForm.reset();
    /* this.loginForm.setValue({
      userData: {
        username: 'admin',
        password: 'admin'
      }
    });*/
    /* this.loginForm.form.patchValue({
      userData: {
        username: 'admin'
      }
    });*/
  }

  login(username: string, password: string) {
    this.store.dispatch(new GetAllUsers());
    this.store.select(getAllUsers())
      .pipe(filter(res => res != null))
      .subscribe((users: any) => {
        users.forEach(data => {
          if (username === data.email && password === data.password) {
            this.route.navigate(['/users/profile']);
            this.store.dispatch(new StoreUserData(data));
            console.log('user logged in');
          } else {
            console.log('username or password incorrect');
          }
        });
      });
    /*this.userService.getUsers().subscribe((users: any[]) => {
      console.log(users);
      users.forEach(data => {
        if (username === data.email && password === data.password) {
          this.route.navigate(['/users/profile']);
          this.store.dispatch(new StoreUserData(data));
          console.log('user logged in');
        } else {
          console.log('username or password incorrect');
        }
      });
    });*/
  }
}
