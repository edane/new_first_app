import {Action} from '@ngrx/store';
import {setAll, SetAllUsers, storeUser, StoreUserData} from './users.actions';

export interface State {
  userData?: any;
  allUsers?: any;
}

export const initialState: State = {
  userData: null,
  allUsers: null
};

export function userReducer(state = initialState, action: Action) {
  switch (action.type) {
    case storeUser:
      return {
        ...state,
        userData: (action as StoreUserData).payload
      };
      break;
    case setAll:
      return {
        ...state,
        allUsers: (action as SetAllUsers).payload
      };
      break;
    default:
      return state;
  }
}
