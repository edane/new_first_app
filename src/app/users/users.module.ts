import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UsersRoutingModule} from './users-routing.module';
import {LoginComponent} from './login/login.component';
import {UsersService} from './login/users.service';
import { ProfileComponent } from './profile/profile.component';
import {ListComponent} from '../shared/list/list.component';
import {WrapTextPipe} from '../shared/pipes/wrap-text.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [LoginComponent, ProfileComponent, ListComponent, WrapTextPipe, RegisterComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [UsersService]
})
export class UsersModule {
}
