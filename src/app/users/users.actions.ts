import {Action} from '@ngrx/store';

export const storeUser = '[User Login] Store User Data';
export const getAll = '[User Login] Get All Users';
export const setAll = '[User Login] Set All Users';

export class StoreUserData implements Action {
  readonly type = storeUser;

  constructor(public payload: any) {
  }
}

export class GetAllUsers implements Action {
  readonly type = getAll;

  constructor() {
  }
}

export class SetAllUsers implements Action {
  readonly type = setAll;

  constructor(public payload: any) {
  }
}
