import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../login/users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

  constructor(private userService: UsersService) {
  }

  ngOnInit() {
    this.registerForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, Validators.required),
      'name': new FormControl(null, Validators.required),
      'birthday': new FormControl(null, Validators.required),
      'address': new FormControl(null, Validators.required),
      'gender': new FormControl('male'),
    });

    this.registerForm.valueChanges.subscribe(value => {
        console.log(value);
      }
    );

    this.registerForm.statusChanges.subscribe(status => {
        console.log(status);
      }
    );
  }

  onSubmit() {
    this.userService.addNewUser(this.registerForm.value).then( onfulfilled => {
      console.log(onfulfilled);
    }, onrejected => {
      console.error('User registration rejected')
    })
    console.log(this.registerForm);

  }
}
