import {State} from './users.reducer';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export const userState = createFeatureSelector<State>('users');

export function getUserData() {
  return createSelector( userState, (state: State) => state.userData);
}

export function getAllUsers() {
  return createSelector( userState, (state: State) => state.allUsers);
}
