import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private db: AngularFirestore) {
  }

  getAllBooks() {
    return this.db.collection('books').valueChanges();
  }
}
