import { Component, OnInit } from '@angular/core';
import {HomeService} from './home.service';
import {Observable} from 'rxjs';
import {Book} from '../shared/models/book';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
 })
export class HomeComponent implements OnInit {
  book$: Observable<Book[]>;
  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.book$ = this.homeService.getAllBooks();
  }

}
