import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'newfirst';

  ngOnInit(): void {
    const observable = new Observable(observer => {
      setInterval(() => {
        observer.next('Hello Observer!');
      }, 5000);

      setTimeout(() => {
        observer.error(new Error('Hello observer Error'));
      }, 11000);

      setTimeout(() => {
        observer.error(new Error('Hello Observer Error'));
      }, 12000);
    });

    observable.subscribe(value => {
      console.log(value);
    }, error => {
      console.error(error);
    }, () => {
      console.log('completed');
    });

    const promise = new Promise(resolve => {
      setTimeout(() => {
        resolve('Hello Promise');
        //Promise.reject('Hello Promise Rejection');
      }, 5000);
    });

    promise.then(onfulfilled => {
        console.log('fulfilled');
      }, onrejected => {
        console.error('rejected');
      }
    );
  }
}
