export interface Book {
 title?: string;
 author?: string;
 description?: string;
 isbn?: string;
 category?: string;
 year?: string;
 nr_copies?: string;
}
