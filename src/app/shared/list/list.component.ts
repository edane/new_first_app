import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() userDetails: any[];

  constructor() {
  }

  ngOnInit() {
  }

  isDate(value) {
    return value instanceof Date;
  }

}
