import {LibraryStateModel} from './library-state.model';
import {ActionReducerMap} from '@ngrx/store';
import {userReducer} from '../../users/users.reducer';

export const libraryReducers: ActionReducerMap<LibraryStateModel> = {
  users: userReducer
}
