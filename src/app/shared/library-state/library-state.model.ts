import {State as UsersState} from '../../users/users.reducer';

export interface LibraryStateModel {
  users: UsersState,
}
