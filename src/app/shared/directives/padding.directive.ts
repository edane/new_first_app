import {Directive, ElementRef, Input, OnChanges} from '@angular/core';

@Directive({
  selector: '[appPadding]'
})
export class PaddingDirective implements OnChanges {
  @Input() padding: string;
  @Input() margin: string;

  constructor(private el: ElementRef) {
  }

  ngOnChanges() {
    this.el.nativeElement.style.padding = this.padding;
    this.el.nativeElement.style.margin = this.margin;
  }
}
