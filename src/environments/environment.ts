// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBg7LwmvJNxHRvWgpMbEzUzNaR2gtzDnoo',
    authDomain: 'amd-training.firebaseapp.com',
    databaseURL: 'https://amd-training.firebaseio.com',
    projectId: 'amd-training',
    storageBucket: 'amd-training.appspot.com',
    messagingSenderId: '940214929562',
    appId: '1:940214929562:web:79f8aa9ee1ad476b'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
